from enum import Enum
from typing import Union

import numpy as np
from numpy.typing import NDArray
from pydantic import BaseModel, Field, PositiveFloat


class Direction(Enum):
    X = "x"
    Y = "y"
    Z = "z"


class Wang(BaseModel):
    width: PositiveFloat
    scale: PositiveFloat

    def theta(self, x: float, y: float) -> list[float]:
        return (
            2.0 * np.arctan2(np.sinh(self.width / self.scale), np.sinh(x / self.scale))
            + np.pi
        )


class Box(BaseModel):
    width: PositiveFloat

    def theta(self, x: float, y: float) -> list[float]:
        if x.abs() < self.width / 2.0:
            return 0.0
        else:
            return np.pi


class DomainWall(BaseModel):
    structure: Union[Wang, Box] = Field(desciption="Structure of the domain wall")
    rotation: float = Field(description="Overall rotation perpendicular change")
    amplitude: float = Field(description="Amplitude of magnetization")
    direction: Direction = Field(description="Direciton of inside and background field")

    def evaluate(self, x: float, y: float) -> NDArray:
        theta = self.structure.theta(x, y)
        if self.direction == Direction.X:
            return self.amplitude * np.array(
                [
                    np.cos(theta),
                    np.sin(theta) * np.cos(self.rotation),
                    np.sin(theta) * np.sin(self.rotation),
                ]
            )
        elif self.direction == Direction.Y:
            return self.amplitude * np.array(
                [
                    np.sin(theta) * np.sin(self.rotation),
                    np.cos(theta),
                    np.sin(theta) * np.cos(self.rotation),
                ]
            )
        elif self.direction == Direction.Z:
            return self.amplitude * np.array(
                [
                    np.sin(theta) * np.cos(self.rotation),
                    np.sin(theta) * np.sin(self.rotation),
                    np.cos(theta),
                ]
            )

    def background(self) -> NDArray:
        if self.direction == Direction.X:
            return np.array([-self.amplitude, 0.0, 0.0])
        elif self.direction == Direction.Y:
            return np.array([0.0, -self.amplitude, 0.0])
        elif self.direction == Direction.Z:
            return np.array([0.0, 0.0, -self.amplitude])


class Array(BaseModel):
    single: DomainWall = Field(description="Element to copy")
    offsets: list[tuple[float, float]] = Field(description="List of offsets")

    def evaluate(self, x: float, y: float) -> NDArray:
        res = np.zeros(3)
        bg = self.single.background()
        for dx, dy in self.offsets:
            res += self.single.evaluate(x + dx, y + dy) - bg
        res += bg
        res /= np.linalg.norm(res)
        res *= self.single.amplitude
        return res

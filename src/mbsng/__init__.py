from enum import Enum
from typing import Optional, Union

import numpy as np
from pydantic import BaseModel, ConfigDict, Field, PositiveFloat, PositiveInt
from scipy.sparse import csc_matrix, lil_matrix

from .magnetization import Array, DomainWall


class Periodic(Enum):
    X = "x"
    Y = "y"
    XY = "xy"


class Hamiltonian(BaseModel):
    model_config = ConfigDict(
        validate_assignment=True,
    )
    size: tuple[PositiveInt, PositiveInt]
    a: PositiveFloat = Field(description="Lattice constant")
    mu: float = Field(description="Chemical potential")
    m: PositiveFloat = Field(description="Mass")
    magnetization: Union[DomainWall, Array] = Field(
        description="Spacially varying magnetic field"
    )
    alpha: float = Field(description="Spin orbit coupling")
    delta: float = Field(description="Superconducting gap")
    magnetic_field: tuple[float, float, float] = Field(
        description="Constant background magnetic field"
    )
    periodic: Optional[Periodic] = Field(
        description="Periodic boundary conditions", default=None
    )

    def periodic_x(self):
        if self.periodic is None:
            return False
        else:
            return self.periodic == Periodic.X or self.periodic == Periodic.XY

    def periodic_y(self):
        if self.periodic is None:
            return False
        else:
            return self.periodic == Periodic.Y or self.periodic == Periodic.XY

    def index(self, x, y):
        return 4 * (x + self.size[0] * y)

    def get_matrix(self) -> csc_matrix:
        t = 1.0 / (2.0 * self.m * self.a**2)
        nx, ny = self.size
        lamb = self.alpha / self.a

        pauli = np.array(
            [
                [[0, 1], [1, 0]],
                [[0, -1j], [1j, 0]],
                [[1, 0], [0, -1]],
            ]
        )
        tau = list(map(lambda m: np.kron(m, np.eye(2)), pauli))
        sigma = list(map(lambda m: np.kron(np.eye(2), m), pauli))
        h = lil_matrix((4 * nx * ny, 4 * nx * ny), dtype=np.complex128)
        for ix in range(nx):
            x = (ix - 0.5 * (nx - 1)) * self.a
            for iy in range(ny):
                ind = self.index(ix, iy)
                y = (iy - 0.5 * (ny - 1)) * self.a
                mag = self.magnetization.evaluate(x, y)

                h[ind : ind + 4, ind : ind + 4] += (4 * t - self.mu) * tau[2]
                h[ind : ind + 4, ind : ind + 4] += self.delta * tau[0]
                for k in range(3):
                    h[ind : ind + 4, ind : ind + 4] += (
                        mag[k] + self.magnetic_field[k]
                    ) * sigma[k]
                if ix > 0 or self.periodic_x():
                    ind2 = self.index((ix + self.size[0] - 1) % self.size[0], iy)
                    h[ind : ind + 4, ind2 : ind2 + 4] -= t * tau[2]
                    h[ind : ind + 4, ind2 : ind2 + 4] -= 0.5j * lamb * sigma[1] @ tau[2]
                    h[ind2 : ind2 + 4, ind : ind + 4] -= t * tau[2]
                    h[ind2 : ind2 + 4, ind : ind + 4] += 0.5j * lamb * sigma[1] @ tau[2]
                if iy > 0 or self.periodic_y():
                    ind2 = self.index(ix, (iy + self.size[1] - 1) % self.size[1])
                    h[ind : ind + 4, ind2 : ind2 + 4] -= t * tau[2]
                    h[ind : ind + 4, ind2 : ind2 + 4] += 0.5j * lamb * sigma[0] @ tau[2]
                    h[ind2 : ind2 + 4, ind : ind + 4] -= t * tau[2]
                    h[ind2 : ind2 + 4, ind : ind + 4] -= 0.5j * lamb * sigma[0] @ tau[2]
        return csc_matrix(h)

    def get_field(self) -> np.ndarray:
        nx, ny = self.size
        res = np.zeros((ny, nx, 3))
        for ix in range(nx):
            x = (ix - 0.5 * (nx - 1)) * self.a
            for iy in range(ny):
                y = (iy - 0.5 * (ny - 1)) * self.a
                res[iy, ix] = self.magnetization.evaluate(x, y) + self.magnetic_field
        return res

    def get_magnetization(self) -> np.ndarray:
        nx, ny = self.size
        res = np.zeros((ny, nx, 3))
        for ix in range(nx):
            x = (ix - 0.5 * (nx - 1)) * self.a
            for iy in range(ny):
                y = (iy - 0.5 * (ny - 1)) * self.a
                res[iy, ix] = self.magnetization.evaluate(x, y)
        return res

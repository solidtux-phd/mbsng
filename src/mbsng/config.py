from pydantic import BaseModel, Field


class Database(BaseModel):
    uri: str = Field(default="localhost:7687")
    user: str = Field(default="neo4j")
    password: str = Field(default="neo4j")

import json
from argparse import ArgumentParser

import matplotlib

from mbsng.config import Database

matplotlib.use("agg")

import os
from copy import deepcopy
from multiprocessing import Pool

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axes_grid1.axes_grid import ImageGrid
from simuldb import Json, Neo4j  # type: ignore
from tqdm import tqdm


def plot(plot_dir, data_dir, id, dx, force, skip_details):
    import warnings

    warnings.filterwarnings("ignore")

    data = np.load(f"{data_dir}/{id}", mmap_mode="r")
    vals_raw = data["vals"]

    element_path = f"{dx:08.4f}"
    mag_path = f"{plot_dir}/mag/{element_path}.png"
    field_path = f"{plot_dir}/field/{element_path}.png"
    vec_path = f"{plot_dir}/vec/{element_path}.png"

    if (
        os.path.exists(mag_path)
        and os.path.exists(field_path)
        and os.path.exists(vec_path)
        and not force
        or skip_details
        or "mag" not in data
        or "field" not in data
        or "vecs" not in data
    ):
        return vals_raw, dx

    for n in ["mag", "field", "vec"]:
        os.makedirs(f"{plot_dir}/{n}", exist_ok=True)
    vecs = data["vecs"]
    vals = deepcopy(vals_raw)
    indices = np.argsort(np.abs(vals))

    for i in range(0, len(vals), 2):
        if vals[indices[i]] > vals[indices[i + 1]]:
            indices[[i, i + 1]] = indices[[i + 1, i]]

    vals = vals[indices]
    num = min(len(vals), 16)
    nx, ny = 0, 0

    for name in ["mag", "field"]:
        mags = data[name]
        mag_path = f"{plot_dir}/{name}/{element_path}.png"
        ny, nx, _ = mags.shape
        fig = plt.figure(figsize=(19.2, 10.8))
        ax = ImageGrid(
            fig,
            111,
            (1, 3),
            share_all=True,
            cbar_mode="each",
            axes_pad=0.1,
            cbar_location="bottom",
        )
        max_mags = np.nanmax(np.abs(mags))
        for i in range(3):
            im = ax[i].imshow(
                mags[:, :, i], vmin=-max_mags, vmax=max_mags, cmap="Spectral"
            )
            ax.cbar_axes[i].colorbar(im)
        fig.suptitle(f"max = {max_mags:.2f}")
        plt.tight_layout()
        plt.savefig(mag_path, dpi=100)
        plt.close()
    vecs = np.reshape(np.transpose(vecs)[indices], (len(vals), ny, nx, 4))
    pol = 2.0 * (
        -vecs[:, :, :, 0] * vecs[:, :, :, 3] + vecs[:, :, :, 1] * vecs[:, :, :, 2]
    )

    fig = plt.figure(figsize=((2 * num / 8) * 19.2, 10.8))
    ax = ImageGrid(fig, 111, (5, 2 * num), share_all=True, axes_pad=0.1)

    ldos = np.sum(np.abs(vecs) ** 2, axis=3)
    max_norm = np.nanmax(ldos)
    max_norm_comp = np.nanmax(np.abs(vecs) ** 2)
    max_pol = np.nanmax(np.abs(pol) ** 2)
    for i in range(num):
        e = vals[i]
        v = vecs[i]
        for j in range(4):
            ax[2 * num * j + i].imshow(
                np.abs(v[:, :, j]) ** 2, aspect=1, vmin=0.0, vmax=max_norm_comp
            )
        ax[2 * num * 4 + i].imshow(ldos[i], aspect=1, vmin=0.0, vmax=max_norm)
        ax[2 * num * 0 + num + i].imshow(np.abs(v[:, :, 0] + v[:, :, 3]) ** 2, aspect=1)
        ax[2 * num * 1 + num + i].imshow(np.abs(v[:, :, 1] + v[:, :, 2]) ** 2, aspect=1)
        ax[2 * num * 2 + num + i].imshow(
            np.real(pol[i]),
            aspect=1,
            vmin=-np.sqrt(max_pol),
            vmax=np.sqrt(max_pol),
            cmap="Spectral",
        )
        ax[2 * num * 3 + num + i].imshow(
            np.imag(pol[i]),
            aspect=1,
            vmin=-np.sqrt(max_pol),
            vmax=np.sqrt(max_pol),
            cmap="Spectral",
        )
        ax[2 * num * 4 + num + i].imshow(
            np.abs(pol[i]) ** 2, aspect=1, vmin=0.0, vmax=max_pol
        )
        ax[i].set_title(f"{e:.2}")
        ax[i + num].set_title(f"{e:.2}")
    fig.suptitle(
        f"max_norm={max_norm:.2} max_pol={max_pol:.2} max_norm_comp={max_norm_comp:.2}"
    )
    plt.tight_layout(pad=0.0, rect=(0.0, 0.0, 1.0, 0.95))
    plt.savefig(vec_path, dpi=100)
    plt.close()
    return vals_raw, dx


def main():
    parser = ArgumentParser(prog="chain_plot")
    parser.add_argument(
        "-j", "--json", help="directory with JSON files", required=False
    )
    parser.add_argument("-d", "--data", help="directory with data", required=True)
    parser.add_argument("-p", "--plot", help="directory for plots", required=True)
    parser.add_argument(
        "-f", "--force", help="overwrite existing plots", action="store_true"
    )
    parser.add_argument(
        "-s", "--skip-details", help="skip single plots", action="store_true"
    )
    parser.add_argument(
        "-b", "--db", help="database config file", default="database.json"
    )
    args = parser.parse_args()

    db = None
    try:
        db_json = json.load(open("database.json"))
        db_config = Database.model_validate(db_json)
        print(db_config)
        db = Neo4j(db_config.uri, db_config.user, db_config.password)
    except:
        pass

    try:
        db = Json(args.json)
    except:
        pass

    if db is None:
        print("No database loaded")
        return
    print(db)

    pool = Pool()
    sessions = db.get_sessions()
    sessions.sort(key=lambda x: x.run.date)
    sessions = sessions[::-1]
    os.makedirs(args.plot, exist_ok=True)
    for session in sessions:
        print(session)
        if session.software.name != "chain":
            continue
        try:
            plot_dir = f"{args.plot}/{session.run.date}"
            plot_vals_path = f"{plot_dir}.png"

            vals_list = []
            dx_list = []
            handles = []
            hosts = set()

            datasets = session.get_datasets()
            print("", f"{len(datasets)} datasets")
            if len(datasets) < 2:
                continue

            for dataset in datasets:
                id = dataset.id
                dx = dataset.metadata["dx"]
                try:
                    hosts.add(dataset.host.hostname)
                except Exception as e:
                    print(e)
                h = pool.apply_async(
                    plot, (plot_dir, args.data, id, dx, args.force, args.skip_details)
                )
                handles.append(h)
                # plot(plot_dir, dx, mags, vecs, vals, pol, FORCE)

            for h in tqdm(handles):
                try:
                    vals, dx = h.get()
                    dx_list.append(dx)
                    vals_list.append(vals)
                except Exception as e:
                    print("", e)

            ham = dataset.metadata["ham"]
            vals = np.asarray(vals_list)
            dx = np.asarray(dx_list)
            ind = np.argsort(dx)
            dx = dx[ind]
            vals = vals[ind]

            fig, ax = plt.subplots(1, 2, figsize=(19.2, 10.8), sharex=True)
            for i in range(vals.shape[1]):
                ax[0].plot(dx, vals[:, i], lw=0.5, alpha=0.5)
                ax[1].semilogy(dx, vals[:, i], lw=0.5, alpha=0.5)
            ax[0].set_prop_cycle(None)
            ax[1].set_prop_cycle(None)
            for i in range(vals.shape[1]):
                ax[0].plot(dx, vals[:, i], "o", ms=1.0)
                ax[1].semilogy(dx, vals[:, i], "o", ms=1.0)
            ax[0].set_xlabel("dx")
            ax[1].set_xlabel("dx")
            ax[0].set_ylabel("E")
            ax[1].set_ylabel("E")
            ax[0].set_ylim(-0.75 * ham["delta"], 0.75 * ham["delta"])
            ax[0].set_xlim(0.0, 10.0)
            ax[1].set_xlim(0.0, 10.0)
            dims = list(map(lambda x: f'{x * ham["a"]:.4f}', ham["size"]))
            fig.suptitle(
                f'{session.run.date} {session.run.id}\n{sorted(list(hosts))}\nsize={ham["size"]} a={ham["a"]} α={ham["alpha"]} μ={ham["mu"]} dim={dims}'
            )
            plt.tight_layout()
            plt.savefig(plot_vals_path, dpi=100)
            print("", f"Saved {plot_vals_path}")
            plt.close(fig)
        except KeyError as e:
            print("", e)
        except ValueError as e:
            print("", e)


if __name__ == "__main__":
    main()

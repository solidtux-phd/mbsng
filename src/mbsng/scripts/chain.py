from pydantic import BaseModel, Field

from .. import Hamiltonian
from ..config import Database
from ..magnetization import Array


class Config(BaseModel):
    ham: Hamiltonian = Field(description="Hamiltonian with single domain wall")
    nev: int = Field(description="Number of eigenvalues", default=8)
    vectors: bool = Field(description="Save vectors", default=True)
    mag: bool = Field(description="Save magnetization", default=True)
    dxrange: tuple[float, float, int] = Field(
        description="Range of wall distances (min, max, steps)"
    )
    num_walls: int = Field(description="Number of walls", default=2)


def main():
    import json
    import os
    import time
    from argparse import ArgumentParser
    from datetime import datetime
    from uuid import uuid4

    import numpy as np
    from mpi4py import MPI
    from scipy.sparse.linalg import eigsh
    from simuldb import Dataset, Json, Neo4j, Run, Session, Software  # type: ignore

    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    args = None
    try:
        if rank == 0:
            parser = ArgumentParser(prog="chain")
            parser.add_argument("-c", "--config", default="config/chain.json")
            parser.add_argument(
                "-j", "--json", help="directory with JSON files", required=False
            )
            parser.add_argument("-d", "--data", help="directory with data")
            parser.add_argument(
                "-b", "--db", help="database config file", default="database.json"
            )
            args = parser.parse_args()
    finally:
        args = comm.bcast(args, root=0)
    if args is None:
        return

    db = None
    try:
        db_json = json.load(open("database.json"))
        db_config = Database.model_validate(db_json)
        db = Neo4j(db_config.uri, db_config.user, db_config.password)
    except:
        pass

    try:
        db = Json(args.json)
    except:
        pass

    if db is None:
        print("No database loaded")
        return
    print(db)

    json_config = json.load(open(args.config))
    config = Config.model_validate(json_config)
    ham = config.ham

    session = None
    date = None
    run_id = None
    if rank == 0:
        date = datetime.now().isoformat(timespec="seconds")
        run_id = Run(date=date).id
    date = comm.bcast(date, root=0)
    run_id = comm.bcast(run_id, root=0)
    run = Run(date, run_id)
    software = Software("chain", "0.2", "")
    session = Session(software, run)

    db_session = db.add_session(session)
    single = ham.magnetization
    os.makedirs(f"{args.data}", exist_ok=True)
    metadata = json_config

    if rank == 0:
        dx_values = np.linspace(*config.dxrange)
        np.random.shuffle(dx_values)
        dx_values = np.array_split(dx_values, size)
    else:
        dx_values = None
    dx_values = comm.scatter(dx_values, root=0)

    start = time.time()
    for i, dx in enumerate(dx_values):
        print(f"{rank:>3}/{size:<3}: [{i+1:>4}/{len(dx_values):<4}] dx={dx:7.4f} start")

        iter_start = time.time()
        metadata["dx"] = dx
        offsets = list(
            map(
                lambda x: (x, 0.0),
                np.linspace(
                    -dx * config.num_walls / 4.0,
                    dx * config.num_walls / 4.0,
                    config.num_walls,
                ),
            )
        )
        ham.magnetization = Array(single=single, offsets=offsets)

        mat_start = time.time()
        mat = ham.get_matrix()
        mat_time = time.time() - mat_start

        field_start = time.time()
        if config.mag:
            mag = ham.get_magnetization()
            field = ham.get_field()
        field_time = time.time() - field_start

        eig_start = time.time()
        if config.vectors:
            vals, vecs = eigsh(
                mat, k=config.nev, which="LM", return_eigenvectors=True, sigma=0.0
            )
        else:
            vals = eigsh(
                mat, k=config.nev, which="LM", return_eigenvectors=False, sigma=0.0
            )
        inds = np.argsort(vals)
        vals = vals[inds]
        if config.vectors:
            vecs = vecs[:, inds]
        eig_time = time.time() - eig_start

        save_start = time.time()
        id = str(uuid4())
        data_path = f"{args.data}/{id}"
        # open the file in order to avoid numpy adding a file ending
        save_data = {"vals": vals}
        if config.vectors:
            save_data["vecs"] = vecs
        if config.mag:
            save_data["mag"] = mag
            save_data["field"] = field
        np.savez(open(data_path, "wb"), **save_data)
        dataset = Dataset(id=id, path=data_path, metadata=metadata)
        db_session.add_dataset(dataset)
        save_time = time.time() - save_start

        iter_time = time.time() - iter_start
        total_elapsed = time.time() - start
        rem = total_elapsed * (len(dx_values) - i - 1) / (i + 1)
        print(
            f"{rank:>3}/{size:<3}: [{i+1:>4}/{len(dx_values):<4}] dx={dx:7.4f} done {iter_time:8.4f}s (mat {mat_time:11.4f}s field {field_time:8.4f}s eig {eig_time:11.4f}s save {save_time:8.4f}s) tot{total_elapsed:11.2f}s ETA {rem:11.2f}s"
        )


if __name__ == "__main__":
    main()

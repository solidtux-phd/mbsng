import json
import os

from pydantic.json_schema import GenerateJsonSchema

from .. import Hamiltonian
from ..config import Database
from ..scripts.chain import Config as Chain

BASE_URL = "https://solidtux-phd.gitlab.io/mbsng"


def main():
    generator = GenerateJsonSchema
    os.makedirs("public", exist_ok=True)
    for name, cls in [
        ("hamiltonian", Hamiltonian),
        ("chain", Chain),
        ("database", Database),
    ]:
        schema = cls.model_json_schema(schema_generator=generator)
        schema["$schema"] = generator.schema_dialect
        schema["$id"] = f"{BASE_URL}/{name}.json"
        with open(f"public/{name}.json", "w") as f:
            f.write(json.dumps(schema, sort_keys=True, indent=4))


if __name__ == "__main__":
    main()

import json
import os
from argparse import ArgumentParser
from copy import deepcopy
from multiprocessing import Pool

import matplotlib.pyplot as plt
import numpy as np
from simuldb import Json, Neo4j

# import matplotlib

# matplotlib.use("agg")


FORCE = False


def main():
    parser = ArgumentParser(prog="chain_alpha")
    parser.add_argument("-d", "--data", help="data dir", default="output/data")
    parser.add_argument("-j", "--json", help="JSON dir")
    parser.add_argument("-p", "--plot", help="output dir", default="output/plots")
    args = parser.parse_args()

    if args.json:
        db = Json(args.json)
    else:
        db = Neo4j("localhost:7687", "neo4j", "password")
    # db = Json("output/json")
    sessions = db.get_sessions()
    sessions.sort(key=lambda x: x.run.date)
    sessions = sessions[::-1]
    value_data = {}
    os.makedirs(f"{args.plot}/alpha", exist_ok=True)

    for session in sessions:
        if session.software.name != "chain":
            continue
        datasets = session.get_datasets(
            # 'match (f)-[:param]->(d2:data) where d2.path = ["vectors"] and d2.value = "false"'
        )
        print("", f"{len(datasets)} datasets")

        for dataset in datasets:
            try:
                alpha = dataset.metadata["ham"]["alpha"]
                dx = dataset.metadata["dx"]
                data = np.load(f"{args.data}/{dataset.id}")

                ham_key = deepcopy(dataset.metadata["ham"])
                del ham_key["alpha"]
                key = json.dumps(ham_key, sort_keys=True)
                alpha_data = value_data.setdefault(key, {}).setdefault(alpha, {})
                alpha_data.setdefault("dx", []).append(dx)
                alpha_data.setdefault("e", []).append(np.nanmin(np.abs(data["vals"])))
            except FileNotFoundError:
                pass
            except Exception as e:
                print(e)

    for i, (ham_key, alpha_data) in enumerate(value_data.items()):
        ham = json.loads(ham_key)

        fig, ax = plt.subplots(1, 2, figsize=(19.2, 10.8))

        alpha_vals = np.asarray(list(alpha_data.keys()))
        data_vals = list(alpha_data.values())
        inds = np.argsort(alpha_vals)
        for k, j in enumerate(inds):
            dx = np.asarray(data_vals[j]["dx"])
            en = np.asarray(data_vals[j]["e"])
            dx_inds = np.argsort(dx)
            dx = dx[dx_inds]
            dx_range = np.nanmax(dx) - np.nanmin(dx)
            en = en[dx_inds]

            enl = np.log10(en)
            m, c = np.polyfit(dx, enl, 1)
            enl = en * np.power(10, -m * dx + c)
            # enl -= m * dx + c
            enf = np.correlate(enl, enl, mode="full")
            enf /= np.nanmax(enf)
            dxf = np.linspace(-dx_range, dx_range, len(enf))
            # print(dxf.shape, en.shape, enf.shape)
            ax[0].semilogy(
                dx,
                en * 10 ** (2 * k),
                label=rf"${alpha_vals[j]:.2f}\ (\cdot 10^{{{2*k}}})$",
            )
            ax[1].semilogy(
                dxf,
                enf * 10 ** (0.2 * k),
                # enf + k,
                label=rf"${alpha_vals[j]:.2f}\ (\cdot 10^{{{2*k}}})$",
            )
        ax[0].legend()
        ax[0].set_xlabel("dx")
        ax[0].set_ylabel("E (with offsets)")
        ax[1].legend()
        ax[1].set_xlabel("k")
        ax[1].set_ylabel("E")

        fig.suptitle(f"a={ham['a']} size={ham['size']} mu={ham['mu']}")
        print(f"a={ham['a']} size={ham['size']} mu={ham['mu']}")
        plt.tight_layout()
        plt.savefig(f"{args.plot}/alpha/{i:05d}.png")
        print(f"{args.plot}/alpha/{i:05d}.png")
        plt.close()


if __name__ == "__main__":
    main()
